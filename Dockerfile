FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    nginx \
    curl \
    nano

COPY cockhex.html /var/www/html/index.nginx-debian.html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
